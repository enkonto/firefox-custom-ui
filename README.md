Fork of [Dook97's](https://github.com/Dook97/firefox-qutebrowser-userchrome) config which is a (soft) fork of [aadilayub's](https://github.com/aadilayub/firefox-i3wm-theme) config.


# A minimalist userChrome.css

Firefox allows a great deal of customization. In fact it goes as far as to give
you the option to completely change the look of the program. That's what
userChrome.css does.

It's goal is to remove all unncessary visual clutter while preserving
functionality. That means you could use this user chrome with an otherwise
normal firefox config, though I recommend you give a try to the
[tridactyl](https://github.com/tridactyl/tridactyl) extension, which I think
goes together great with this user chrome.

Navbar isn't completely removed; it shows on focus (hover or `Ctrl+L`).
Color of text in tabs is determined by the color code of the tab's container.


## How to use

Enable user chrome in `about:config`:
1) in `about:config` set `toolkit.legacyUserProfileCustomizations.stylesheets`
to `true`

Enable compact mode in `about:config`:
1) in `about:config` set `browser.compactmode.show` to `true`
2) in the customize toolbar menu set `density` to `compact`

1) Clone this repo.
2) Copy or make symlinks to ``autoconfig.js'' into the ``defaults/pref''
directory and to ``my_autoconfig.cfg'' at the top level of the Firefox directory.
3) Make a symlink to the `userChrome.css` or copy it to the `chrome` directory
in preferred firefox profile. If there is none create it.
4) Enable dark theme in settings if it's not Librewolf.

If you're not sure, what is the path to your profile's directory, you can find
out by going to `about:profiles`

If you want the navbar to show on hover go to the bottom of the file and
uncomment the code there. Otherwise you may only toggle the navbar by pressing
`Ctrl+L`

Change in `about:config` `font.size.systemFontScale` as you prefer. For
instance, 120 is ok for 14" FHD and 150 is ok for 28" 4K.

## Common issues

### black stripe under tab bar

Increase the value of the `--tab-min-height` variable in userChrome.css.

### invisible button at the end of urlbar

This is intentional - it preserves the `Ctrl-D` functionality. If you don't
need that, you can remove it in the code. Search for `star-button-box` and
uncomment the line in question.

### I want favicons

Search for 'disable favicons' and comment out the relevant line.

## Is it practical?

Absolutely - you just have to get comfortable using keyboard shortcuts. I use it
with the aforementioned Tridactyl extension, which pushes the experience to a
whole new level.

Some tips:

* On linux there will often be a key, which toggles window moving mode.
  Oftentimes it's left `Alt`.
* `Ctrl+shift+W` or `Ctrl+Q` closes Firefox
* `Ctrl+L` toggles the navbar
* `Ctrl+F Esc` will defocus urlbar.
